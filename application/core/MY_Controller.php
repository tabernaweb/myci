<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MyCI Core
 *
 * All controllers is passed by here, login verification and default data to view files
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class MY_Controller extends CI_Controller 
{

	public $dataView = array();

	public function __construct()
	{
		parent::__construct();
		
		if($this->uri->segment(1)=='admin')
		{
			$this->dataView['title'] = 'Admin';
			
			if(!$this->session->userdata('logged'))
			{
				if($this->uri->segment(2)!='login')
				{
					redirect('admin/login');
				}
			} 
			else 
			{
				$this->dataView['currentPage'] = $this->uri->segment(2);
				$this->dataView['user'] = $this->session->userdata('user');
			}
		}
                
                if($this->session->userdata('lang'))
                {
                    $this->lang->is_loaded = array();
                    $this->lang->language = array();
                    $this->lang->load('app', $this->session->userdata('lang'));
                }

        $this->dataView['lang_active'] = $this->lang->line('lang_code');
		$this->dataView['title'] = $this->lang->line('default_title');
        $this->dataView['currentController'] = $this->uri->segment(1);

	}

	public function bladeView($page, $data=NULL)
	{
		echo $this->blade->view()->make($page, $data)->render();
	}

}
